package citi.service.carshare;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CarShareServiceTests {

    @Test
    @DisplayName("herbyNumberTest")
    public void herbyNumberTest(){
        //CarShareService carShareService = new CarShareService();
        assertEquals(53, CarShareService.getHerbyNumber(), "Herby's number should be 53");
    }
}